\documentclass[conference, 12pt]{IEEEtran}
\usepackage{graphicx}			% Inclusão de gráficos
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amssymb}

\title{Amplificadores operacionais:\\Circuitos de detecção de imperfeições, integrador e seguidor}
\author{\IEEEauthorblockN {Andrei Alexei Levy Buslik} \IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB} \and \IEEEauthorblockN {Igor Fernandes Miranda}
\IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB}}


\begin{document}
\maketitle
\abstractname{--}\textbf{Este relatório visa comparar, através de cálculos, simulações e experimentos práticos, circuitos que utilizam amplificadores operacionais. Inicialmente, é feita a descrição de um amplificador operacional, em seguida é mostrada uma simulação de um circuito, cálculos para resultados esperados em outros circuitos e, por fim, comparam-se os resultados teóricos e simulados com os resultados dos experimentos em laboratório.}

\section{\textbf{Introdução}}
\quad Amplificador operacional ou amp-op é um amplificador que pode proporcionar ganhos de tensão ou de corrente, seu interior é constituído de transistores e resistores. Inicialmente foram construídos para realizar operações matemáticas como adição, subtração, integração e derivação, contudo, incorporando-se redes simples podem ser construídos "blocos de operações" como ganho de tensão ou conversão de corrente para tensão.

\quad Um modelo para um amp-op pode ser definido como uma fonte de tensão controlada por de tensão tendo um fator de ganho $A$, uma resistência de entrada $R_{in}$ e uma resistência de saída $R_{o}$. A Figura 1 mostra o modelo descrito.
\begin{figure}[h]
\includegraphics{Figura1_DCE}
\caption{}
\end{figure}

\quad A entrada $V_{-}$ é chamada de \textit{inversora}, já a entrada $V_{+}$ é chamada \textit{não-inversora}. Um aplificador ideal possui resistência de entrada $R_{in} = \infty$, resistência de saída $R_{o} = 0$, ganho e largura de banda infinitos, função de transferência linear e inalterabilidade de módulo e fase entre as tensões de entrada e saída. Já um amp-op real possui certas limitações, como ganho finito, impedância de entrada finita e impedância de saída maior que zero, e imperfeições que estão listadas a seguir:
\begin{itemize}
\item Imperfeições em corrente contínua
	\begin{enumerate}
		\item \textbf{Tensão de \textit{offset}} - o amp op irá produzir uma tensão de saída mesmo que os pinos de entrada estejam com exatamente a mesma voltagem.
		\item \textbf{Correntes de \textit{offset} e de polarização de entrada} - os terminais de entrada devem obrigatoriamente estar alimentados por correntes contínuas, denominadas \textbf{correntes de polarização de entrada}.
	\end{enumerate}
\end{itemize}

\quad Podemos destacar três configurações básicas de circuitos que utilizam amp-ops. A primeira é um comparador de tensão que consiste de um amp-op de alto ganho, sem realimentação. A Figura 2 mostra um comparador de tensão. Podemos comparar $V_+$ e $V_-$ e ver qual deles é maior, se o ganho for positivo $V_+ > V_-$, caso contrário $V_+ < V_-$.
\begin{figure}[ht]
\includegraphics{Figura2_DCE}
\caption{}
\end{figure}

\quad Outra configuração possível mas com realimentação é a configuração inversora que está representada na Figura 3. Nessa configuração vemos que a entrada não-inversora é ligada ao terra e seu ganho é dado por $A = - \frac{R_2}{R_1}$.
\begin{figure}[ht]
\includegraphics{Figura3_DCE}
\caption{}
\end{figure}

\quad Semalhante ao circuito anterior, podemos realimentar o circuito mas com a entrada inversora ligada ao terra, essa configuração é conhecida como não-inversora e está demonstrada na Figura 4. Para esse tipo de amplificador o ganho é de $A = (1 + \frac{R_f}{R_1})$.
\begin{figure}[ht]
\includegraphics[scale = 0.7]{Figura4_DCE}
\caption{}
\end{figure}

\quad Podemos montar um circuito comparador tanto na sua forma inversora como na sua forma não-inversora. A Figura 5 mostra um exemplo de comparador de tensão na forma inversora. Quando a tensão de entrada, aplicada à entrada não inversora (+), é menor do que a tensão de referência ($V_{ref}$), a saída se mantém no nível baixo, ou seja, perto de 0 V. Por outro lado, quando a tensão de entrada é maior do que a tensão de referência, a tensão de saída vai ao nível alto, ou seja, próxima da tensão de alimentação. Quando as tensões se igualam temos uma condição de instabilidade, dado o alto ganho do circuito, o que quer dizer que numa faixa muito estreita de tensões de entrada em que ela se aproxima e passa de Vref, temos a transição do circuito.
\begin{figure}[ht]
\includegraphics{Figura5_DCE}
\caption{}
\end{figure}
\quad Considerando o que foi discutido até aqui passaremos para a parte de cálculos teóricos e simulação de circuitos utilizando amp-op.

\section{\textbf{Questões teóricas e simulação}}
\quad Nesta sessão serão respondidas quatro questões utilizando amp-ops. A primeira questão pede para montar, utilizando um software de simulação, um amplificador de tensão, na configuração inversora e com ganho $A = -10 \frac{V}{V}$, esboçando a saída do circuito considerando uma entrada senoidal de amplitude $10 mV$.
\quad Utilizando o software \textit{Circuit Maker}, o circuito foi montado conforme mostra a Figura 6. \textbf{Uma vez que queremos montar um circuito inversor, sabemos que ser ganho é dado por $A = - \frac{R_2}{R_1}$. Como queremos que seu ganho seja $A = -10 \frac{V}{V}$, convenientemente atribuimos que $R_2 = 10 k\Omega$ e $R_1 = 1 k\Omega$. Assim, como podemos ver no gráfico presente na Figura 6, a tensão de saída (onda em azul) tem fase oposta a da tensão de entrada (onda em vermelho) e amplitude dez vezes maior}.
\begin{figure}[ht]
\includegraphics[scale = 0.58]{Figura6_DCE}
\caption{}
\end{figure}

\quad A segunda questão diz que, dados os circuitos da Figura 7, a tensão de \textit{offset} $V_{os}$ e as correntes de polarização das entradas inversora ($I_{B1}$) e não-inversora ($I_{B2}$) podem ser estimadas. Pede-se para explicar o funcionamento desses circuitos e como estimar as imperfeições a partir de medições de $V_{S1}$, $V_{S2}$, $V_{S3}$ e propor um circuito que minimize os efeitos das imperfeições $V_{os}$, $I_{B1}$ e $I_{B2}$. 
\begin{figure}[ht]
\includegraphics[scale = 0.7]{Figura7_DCE}
\caption{}
\end{figure}

\quad O circuito da Figura 7a é obtido de uma configuração inversora curto-circuitando a entrada. Dessa forma, para analisar os efeitos da tensão de \textit{offset}, o circuito pode ser visto na Figura 8.
\begin{figure}[ht]
\includegraphics{Figura8_DCE}
\caption{}
\end{figure}

\quad Pelo circuito da Figura 8 temos uma configuração não-inversora, assim: $$V_{S1} = V_{os}(1 + \frac{R_2}{R_1})$$

\quad Dessa forma, digamos que o ganho fosse de $1000 \frac{V}{V}$, para uma tensão de \textit{offset} de $5 mV$, a saída $V_{S1}$ poderia ser de $+5 V$ ou $-5 V$, dependendo da polaridade de $V_{os}$, em vez de $0 V$. \textbf{Uma forma de superar este problema é ligar um capacitor entre o terra e o resistor $R_1$, fazendo com que o ganho em corrente contínua seja zero. O acoplamento do capacitor faz com que o circuito equivalente seja visto como o circuito da Figura 7b. Nesse caso temos que $$V_{S2} =  V_{OS}$$}

\quad Para as correntes de polarização, o circuito da Figura 7a é visto como o da Figura 9. Elas são representadas pelas fontes de corrente independentes. Geralmente, o valor médio e a diferença entre elas são especificados pelos fabricantes. O valor médio $I_B$ (corrente de polarização de entrada) e a diferença (corrente de \textit{offset} de entrada) das correntes de polarização são: $$I_B = \frac{I_{B1} + I_{B2}}{2}$$ $$I_{OS} = |I_{B1} - I_{B2}|$$
\begin{figure}[htpb]
\includegraphics[scale=0.9]{Figura9_DCE}
\caption{}
\end{figure}

\quad Podemos notar pela Figura 9 que $$V_{S1} = I_{B1}R_2 \simeq I_BR_2$$

\quad Isso limitaria o valor máximo de $R_2$. Para que isso não ocorra um procedimento utilizado é conectar um resistor $R_3$ em série com a entrada não inversora, dessa forma $$V_{S1} = -I_{B2}R_3 + R2(I_{B1} - I_{B2}\frac{R_3}{R_1)}$$ Considerando o caso em que $I_{B1} = I_{B2} = I_B$ $$V_{S1} = I_B[R_2 - R_3(1 + \frac{R_2}{R_1})]$$

\quad \textbf{Podemos escolher $R_3$ de modo a reduzir $V_{S1}$ a zero. Basta que $$R_3 = \frac{R_2}{1 + \frac{R_2}{R_1}} = \frac{R_1R_2}{R_1 + R_2}$$}

\quad Agora que $R_3$ foi selecionado, a partir das equações $$I_B = \frac{I_{B1} + I_{B2}}{2}$$ $$I_{OS} = |I_{B1} - I_{B2}|$$ chegamos a conclusão que, dado $I_{OS}$ finita, $I_{B1} = I_B + \frac{I_{OS}}{2}$ e $I_{B2} = I_B - \frac{I_{OS}}{2}$. Assim, substituindo esses valores em $$V_{S1} = -I_{B2}R_3 + R2(I_{B1} - I_{B2}\frac{R_3}{R_1)}$$ obtemos a equação $$V_{S1} =  I_{OS}R_2$$

\quad Logo, \textbf{para minimizar os efeitos das correntes de polarização} chegamos ao circuito da Figura 7b, \textbf{com $R_3$ tendo o mesmo valor que $R_2$}.

%Questão 2
A figura \ref{fig:Integrador de Miller} apresenta o integrador de Miller, com a entrada conectada a 0V. 

\begin{figure}[htpb]
    \centering
    \includegraphics[keepaspectratio=true, scale=1]{Integrador_Miller.png}
    \caption{Integrador de Miller}
    \label{fig:Integrador de Miller}
\end{figure}

A primeira vista, a tensão de saída V\textsubscript{s} deve permanecer em 0V. Porem, devido a tensão e corrente de offset (V\textsubscript{os}, I\textsubscript{B1} e I\textsubscript{B2}) isso não ocorre. Analisando esse circuito com com a tensão e corrente de offset separadamente teremos as seguintes tensões de saída:

$$V_{o} = V_{os} + \frac{V_{os}}{CR}t$$

$$V_{o} = I_{B2}R + \frac{I_{B1}}{C}t$$

Logo por superposição temos:

$$V_{o} = V_{os} + \frac{V_{os}}{CR}t + I_{B2}R + \frac{I_{B1}}{C}t$$

Admitindo por simplicidade que no estante t=0 a tensão do capacitor é zero, teremos que a tensão $$V_{o} = V_{os} + I_{B2}R$$. 

Após o instante t=0 V\textsubscript{o} ira aumentar linearmente até que o Amp Op fique saturado.  


\section{\textbf{Experiência}}

Montamos o circuito da Questão 1 com \textit{R}\textsubscript{1}=1\textit{M} $\Omega$ e \textit{R}\textsubscript{2} = 10\textit{M} $\Omega$ , e obtivemos o resultado mostrado na figura \ref{fig:EQ1a}.

\begin{figure}[htpb]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.05]{Q1.png}
    \caption{Amplificador inversor com ganho de -10}
    \label{fig:EQ1a}
\end{figure}

Ao observarmos o dado obtido podemos concluir que tivemos um ganho de -10, com a entrada em 50m V e a saida em 500m V. O resultado obtido foi exatamente igual ao da teoria, onde o ganho do amplificador inversor é dado por:

$$A = - \frac{R_2}{R_1}$$

O ruido ocasionado na saída é devido as correntes de polarização existentes no amplificador operacional real. Para minimizar esse efeito introduzimos um resistor de valor igual ao paralelo entre R\textsubscript{1} e R\textsubscript{2} na entrada não inversora do amplificador, assim obtivemos uma atenuação do ruido na saída como apresentado na figura \ref{fig:EQ1c}.

\begin{figure}[htpb]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.05]{Q1c.jpg}
    \caption{Atenuação do ruido na saída}
    \label{fig:EQ1c}
\end{figure}


Para provarmos a existência das correntes de polarização e tensão de offset nos amplificadores reais utilizamos o Integrador de Miller mostrado na figura \ref{fig:Integrador de Miller} com \textit{R}=10 \textit{M} $\Omega$ e C =10 \textit{n}F. Na teoria provamos que mesmo com as entradas do amplificador aterradas ele deve saturar devido as correntes de polarização e tensão de offset, e foi exatamente isso que ocorreu no experimento. O LM741 saturou em aproximadamente  2,11 segundos e o TL074



\end{document}


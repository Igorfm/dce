\documentclass[conference, 12pt]{IEEEtran}
\usepackage{graphicx}			% Inclusão de gráficos
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amssymb}
\bibliographystyle{abnt-num}
\usepackage{float}

\title{Regulador de tensão}
\author{\IEEEauthorblockN {Andrei Alexei Levy Buslik \\ 11$\backslash$0024702} \IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB \\ } \and \IEEEauthorblockN {Igor Fernandes Miranda \\ 11$\backslash$0013255}
\IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB}}


\begin{document}
\maketitle
\abstractname{--}\textbf{Este relatório visa comparar, através de cálculos, simulações e experimentos práticos, circuitos que utilizam transistores TBJ. Inicialmente é descrita sua estrutura interna e seu comportamento, em seguida são feitas questões teóricas e simulações bem como experimentos em laboratório. Por fim, são comparados os resultados teóricos, simulados e de experimento e é feita uma análise entre os resultados.}

\section{\textbf{Introdução}}

\quad Transitores Bipolares de Junção (TBJ) consiste em três regiões semicondutoras:
\begin{itemize}
\item região do emissor
\item região da base
\item região do coletor
\end{itemize}
podendo ser do tipo \textit{npn} ou \textit{pnp}.

\quad Dependendo da condição de polarização (direta ou inversa) das junções base-coletor (JBC) e base-emissor (JBE) são obtidos três diferentes modos de operação, vistos na Tabela \ref{T1}.

\begin{table}[!h]
\setlength{\tabcolsep}{25pt} 
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Modo} & \textbf{JBE} & \textbf{JBC}\\
\hline
Corte & Reversa & Reversa\\
\hline
Ativo & Direta & Reversa\\
\hline
Saturação & Direta & Direta\\
\hline
\end{tabular}
\end{center}
\caption{Modos de operação do TBJ.}
\label{T1}
\end{table}

\quad O \textbf{modo ativo} é utilizado para que o transistor opere como amplificador, como visto na Figura \ref{TBJamp}; já as aplicações de chaveamento, como visto na Figura \ref{TBJchave}, utilizam os \textbf{modos de corte} e \textbf{saturação}.

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.6]{TBJamp.png}
    \caption{Circuito utilizando TBJ tipo \textit{npn} como amplificador.}
   \label{TBJamp}
\end{figure} 

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{TBJchave.png}
    \caption{Circuito utilizando TBJ tipo \textit{npn} como chave.}
    \label{TBJchave}
\end{figure} 

\quad Os circuitos que utilizam TBJ como aplificadores possuem um desafio inerente ao funcionamento desse tipo de transistor. Para que o TBJ opere como amplificador linear é necessário que ele seja polarizado em um ponto da região ativa, o desafio é encontrar esse ponto.

\section{\textbf{Questões teóricas e simulação}}

\quad Nesta sessão responderemos à questões teóricas referentes ao circuito da Figura \ref{Circuito1}.

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.6]{Circuito1.png}
    \caption{Amplificador transistorizado.}
    \label{Circuito1}
\end{figure} 

\subsection{\textbf{Questão 1(a):}}
\quad O circuito do amplificador transistorizado da Figura \ref{Circuito1} tem os capacitores e fontes DC independentes curto-circuitados de forma que o circuito AC equivalente é visto na Figura \ref{fig:1a}.

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.8]{1a.png}
    \caption{Circuito equivalente AC do amplificador transistorizado.}
    \label{fig:1a}
\end{figure} 

\subsection{\textbf{Questão 1(b):}}

\quad A partir do circuito da Figura \ref{fig:1a} podemos montar outro circuito equivalente para determinar-mos a tensão de saída $V_o(t)$, as impedâncias de entrada $R_i$ e de saída $R_o$.
 
\quad O circuito $\pi$-Híbrido equivalente é apresentado na Figura \ref{fig:PI}. Nele vemos uma fonte de corrente dependente ($I$) e a resistência interna do transistor $r_\pi$.

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.45]{PI.png}
    \caption{Circuito equivalente PI Híbrido  AC do amplificador transistorizado.}
    \label{fig:PI}
\end{figure} 

\quad Temos que $I = gm \times V_{BE}$ e $r_\pi = \frac{V_T}{i_B}$, onde $V_{BE}$ é a tensão entre a base e o emissor, $R_C // R_L$ sendo o paralelo entre as resistências $R_C$ e $R_L$, $i_B$ a corrente na base do transistor e $V_T$ é um parâmetro intrínseco a cada transistor. Dessa forma, as fórmulas literais obtidas foram:

\begin{equation}\label{Eq1}
V_o = -gm \times V_{BE} \times (R_C//R_L)
\end{equation}

\begin{equation}\label{Eq2}
 V_{BE} = \frac{R_{B1}//R_{B2}//r_{\pi}}{(R_{B1}//R_{B2}//r_{\pi}) + R_S} \times V_S
\end{equation}

\begin{equation}\label{Eq3}
A_v = \frac{V_o}{V_S} = -gm \times \frac{R_{B1}//R_{B2}//r_{\pi}}{(R_{B1}//R_{B2}//r_{\pi}) + R_S} \times (R_C//R_L)
\end{equation}

\begin{equation}\label{Eq4}
gm = \frac{i_C}{V_T} = \frac{\beta \times i_B}{V_T} = \frac{\beta}{\beta + 1} \times \frac{i_E}{V_T}
\end{equation}

\begin{equation}\label{Eq5}
R_i = R_{B1} // R_{B2} // r_{\pi}
\end{equation}

\begin{equation}\label{Eq6}
R_o = R_C
\end{equation}

\subsection{\textbf{Questão 1(c):}}

\quad O ganho entre a tensão de entrada e saída é dado pela Equação \ref{Eq3}, dessa forma:
\begin{itemize}
\item[•]Para $\beta = 100$:
$$A_v \simeq -123V/V$$
\item[•]Para $\beta = 800$:
$$A_v \simeq -1025V/V$$
\end{itemize}

\subsection{\textbf{Questão 1(d):}}

\quad Por fim,a Figura \ref{SimCircFig1} mostra a simulação do circuito da Figura \ref{Circuito1} utilizando o programa \textit{Circuit Maker}, temos o sinal de entrada aumentado em 20 vezes (em verde) e o sinal de saída (em azul).

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.45]{SimCirc1.png}
    \caption{Simulação do circuito da Figura \ref{Circuito1}.}
    \label{SimCircFig1}
\end{figure} 

\section{\textbf{Experimento}}

\quad O experimento foi iniciado com a montagem do circuito da Figura \ref{Circuito1}, os parâmetros estão listados na Tabela \ref{T2}.

\begin{table}[!h]
\setlength{\tabcolsep}{25pt} 
\begin{center}
\begin{tabular}{|c|c|}
\hline
\textbf{Componente} & \textbf{Valor} \\
\hline
$V_{cc}$ & $12V$ \\
\hline
$R_{b1}$ & $56k\Omega$ \\
\hline
$R_{b2}$ & $36k\Omega$ \\
\hline
$RE$ & $4,7k\Omega$ \\
\hline
$RC$ & $4,7k\Omega$ \\
\hline
$RS$ & $1k\Omega$  \\
\hline
$RL$ & $47k\Omega$ \\
\hline
$C_1$ & $4,7\mu F$ \\
\hline
$C_2$ & $4,7\mu F$ \\
\hline
$C_3$ & $4,7\mu F$ \\
\hline
$T_1$ & BC548 \\
\hline
\end{tabular}
\end{center}
\caption{Parâmetros para o circuito da Figura \ref{Circuito1}.}
\label{T2}
\end{table}

\quad Para $V_S(t)$ inicialmente foi utilizado um gerador de sinal senoidal com amplitude de $20mV$ e frequência de $1kHz$. A Figura \ref{ExpCirc1} mostra as ondas de entrada (em verde) e saída (em amarelo) medidas no osciloscópio. Em seguida, a amplitude do sinal de entrada foi aumentada de $20mV$ para $40mV$ e a resistência $RS$ foi levada à zero. Com essas mudanças e mantendo $RL = 47k\Omega$, vemos as novas ondas de entrada e saída na Figura \ref{RL47}. Já a Figura \ref{RLInf} mostra o mesmo circuito porém com $RL = \infty$.

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{OutCirc1.png}
    \caption{Sinais de entrada e saída do circuito da Figura \ref{Circuito1}.}
    \label{ExpCirc1}
\end{figure} 

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{OutRL.jpg}
    \caption{Sinais de entrada e saída do circuito da Figura \ref{Circuito1} com modificação nos parâmetros $V_S(t)$ e $RS$, para $RL = 47 k\Omega$.}
    \label{RL47}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{OutRLIfnity.jpg}
    \caption{Sinais de entrada e saída do circuito da Figura \ref{Circuito1} com modificação nos parâmetros $V_S(t)$ e $RS$, para $RL = \infty$.}
    \label{RLInf}
\end{figure}

\quad Após as últimas leituras feitas para as ondas de entrada e saída, a frequência do sinal de entrada foi levada para $1Hz$ e foi sendo aumentada gradativamente para que se pudesse estimar a faixa de operação do transistor. A Tabela \ref{T3} mostra as frequências em que o TBJ começa a amplificar até o momento em que ele passa a distorcer o sinal.

\begin{table}[!h]
\setlength{\tabcolsep}{5pt} 
\begin{center}
\begin{tabular}{|c|c|}
\hline
\textbf{Frequência de corte inferior} & \textbf{Frequência de corte superior} \\
\hline
$450Hz$ & $2.2kHz$ \\
\hline
\end{tabular}
\end{center}
\caption{Faixa de frequências de funcionamento do TBJ.}
\label{T3}
\end{table}

\quad Seguindo o experimento, o circuito da Figura \ref{Circuito2} foi montado a partir do circuito da Figura \ref{Circuito1}. Os parâmetros dos novos componentes são mostrados na Tabela \ref{T4}. A Figura \ref{Circ2} mostra o circuito montado.

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.6]{Circuito2.png}
    \caption{Amplifcador de sinais de voz com estágio de saída \textit{push-pull}.}
    \label{Circuito2}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{Circ2Exp.jpg}
    \caption{Circuito montado a partir do circuito da Figura \ref{Circuito2}.}
    \label{Circ2}
\end{figure}

\begin{table}[!h]
\setlength{\tabcolsep}{25pt} 
\begin{center}
\begin{tabular}{|c|c|}
\hline
\textbf{Componente} & \textbf{Valor} \\
\hline
$C_4$ & $220\mu F$ \\
\hline
$RM$ & $1k\Omega$ \\
\hline
$R_{a1}$ & $100k\Omega$ \\
\hline
$R_{a2}$ & $100k\Omega$ \\
\hline
$T_2$ & TIP31 \\
\hline
$T_3$ & TIP32 \\
\hline
\end{tabular}
\end{center}
\caption{Parâmetros para o circuito da Figura \ref{Circuito2}.}
\label{T4}
\end{table}

\section{\textbf{Questões experimentais e discussão}}

\quad Pela Figura \ref{ExpCirc1} estima-se um ganho entre o sinal de entrada e saída em torno de $-60V/V$, na subseção \textit{C} da seção \textit{II} vimos que o ganho esperado para $\beta = 100$ era de $-123V/V$ e para $\beta = 800$ era de $-1075V/V$. Vemos que o ganho do experimento não ficou na faixa do ganho teórico, isso pode ser devido ao parâmetro $V_T$ do TBJ, para os cálculos teóricos foi usado o valor de $25mV$ e esse valor muda com a temperatura.

\quad Aumentado a amplitude do sinal de entrada e fazendo $R_S = 0$ não foi notada nenhuma diferença aparente na relação entre as tensões de entrada e saída, porém, ao fazer $R_L = \infty$ nota-se um aumento no ganho. A diferença pode ser vista comparando-se a Figura \ref{RL47} e a Figura \ref{RLInf}.

\quad A tabela \ref{T3} mostra que o transistor começa a amplificar o sinal em frequências próximas à $450 Hz$ e começa a cortar o sinal na saída em frequências a cima de $2.2kHz$. O comportamento de um amplificador em função da sua faixa de freqüência de operação está relacionado, nas freqüências mais baixas, com o valor dos capacitores de acoplamento ($C_1$) e desacoplamento ($C_3$) e, nas freqüências
mais altas, com as capacitâncias parasitas para junções base-coletor (JBC) e
base-emissor (JBE), fornecidas pelos manuais dos fabricantes.

\quad Por fim, o circuito da Figura \ref{Circ2} apresentou muito ruído na saída do auto-falante, o que pode ser devido à interferência do protoboard ou mesmo no microfone. Também foi notado que o som no auto-falante não estava muito bem amplificado, de fato parecia estar mais baixo que o som da fonte sonora utilizada no microfone.

\section{\textbf{Referências}}
[1] A.S. Sedra e K. Smith, Microeletronica, 5a ed. Pearson, Brasil, 2007.

[2] $http://fhollweg.xpg.uol.com.br/eletronica_cap2.pdf$

[3] $http://lilith.fisica.ufmg.br//roteirosPDF/Diodo_semicondutor.pdf$

\bibliographystyle{abbrv}
\end{document}

%http://fhollweg.xpg.uol.com.br/eletronica/eletronica_cap2.pdf
%http://lilith.fisica.ufmg.br/~labexp/roteirosPDF/Diodo_semicondutor.pdf

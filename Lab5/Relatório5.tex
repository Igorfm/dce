\documentclass[conference, 12pt]{IEEEtran}
\usepackage{graphicx}			% Inclusão de gráficos
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amssymb}
\bibliographystyle{abnt-num}

\title{Caracterização de transistores MOS}
\author{\IEEEauthorblockN {Andrei Alexei Levy Buslik \\ 11$\backslash$0024702} \IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB \\ } \and \IEEEauthorblockN {Igor Fernandes Miranda \\ 11$\backslash$0013255}
\IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB}}


\begin{document}
\maketitle
\abstractname{--}\textbf{Este relatório visa comparar, através de cálculos, simulações e experimentos práticos, o comportamento do transistor MOS. Inicialmente é descrita sua estrutura interna e seu comportamento, em seguida são feitas questões teóricas e simulações bem como experimentos em laboratório. Por fim, são comparados os resultados teóricos, simulados e de experimento e é feita uma análise entre os resultados.}

\section{\textbf{Introdução}}
\quad O MOS é um dispositivo semicondutor de efeito de campo, são usados em vários circuitos para conversão de tensões. Ele tem quatro terminais de semicondutores chamados de fonte(source), porta(gate), dreno(drain) e subtrato(body). A fonte e o dreno estão localizados no substrato do transistor, enquanto a porta está acima desses três terminais, entre o corpo e o dreno, separado dos outros terminais por uma camada isolante. Porém, normalmente apenas três terminais são acessíveis, com o substrato ligado a fonte. Há dois tipos essenciais de transistores MOS, o de canal N e o de canal P, e se diferenciam basicamente pela polarização.

\quad Esse dispositivos são controlados por tensão. Quando aplica-se uma tensão entre a porta e a fonte um campo elétrico é gerado, esse campo forma uma espécie de canal invertido que possibilita a passagem de corrente no transistor. Assim, com a variação da tensão entre a porta e a fonte, é possível controlar o fluxo de corrente entre o dreno e a fonte.

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{Estrutura_MOSFET.png}
    \caption{Estrutura de um MOSFET.}
    \label{fig:Estrutura_MOSFET}
\end{figure}

\quad A operação do MOS pode ser dividido em três modos:

\begin{itemize}
\item \textbf{Região de Corte:} Nesse modo o transistor permanece desligado não existindo corrente entre o dreno;
\item \textbf{Região de triodo:} Nesse modo o transistor está "ligado", é criado um canal que permite um fluxo de corrente entre o dreno e a fonte. O MOS funciona como um resistor, com resistência controlada pela tensão entre o dreno e a fonte ($V_{DS}$).
\item \textbf{Região de saturação:} Nesse modo existe corrente passando pelo transistor assim como no modo  triodo. Porém, a tensão $V_{DS}$ não afeta o crescimento da corrente entre o dreno e a fonte.
\end{itemize}


\quad O transistor MOS é utilizado em vários tipos de circuitos, podemos utiliza-lo como chave (Figura \ref{fig:MOSFET_chave}), em circuitos de espelhamento de corrente (Figura \ref{fig:MOSFET_espelho_corrente}) ou como amplificador (Figura \ref{fig:MOSFET_Amp_Fonte}).

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{MOSFET_chave.png}
    \caption{MOSFET como chave de uma lâmpada.}
    \label{fig:MOSFET_chave}
\end{figure}  

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{Espelho_Corrente.png}
    \caption{MOSFET como espelho de corrente.}
    \label{fig:MOSFET_espelho_corrente}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{Amplificador_fonte_comum.png}
    \caption{MOSFET como amplificador de fonte comum.}
    \label{fig:MOSFET_Amp_Fonte}
\end{figure} 

\section{\textbf{Questões teóricas}}
\quad Usando como referência o circuito da Figura \ref{fig:Circuito_exp} foram calculados possíveis valores para a corrente $I_D$. Para isso é necessário levar em conta:
\begin{itemize}
\item \textbf{Modo de Corte:}
\begin{itemize}
\item $V_{GS} < V_t$
\item $I_D = 0$
\end{itemize}
\item \textbf{Modo Triodo:}
\begin{itemize}
\item $V_{GS} > V_t$ e $V_{DS} < V_{GS} - V_t$
\item $I_D = \frac{\beta_n}{2}(V_{GS} - V_t)V_{DS} - V_{DS}^2$
\end{itemize}
\item \textbf{Modo Saturação:}
\begin{itemize}
\item $V_{GS} > V_t$ e $ V_{DS} \geq V_{GS} - V_t$
\item $I_D = \frac{\beta_n}{4}(V_{GS} - V_t)^2$
\end{itemize}
\end{itemize}

Onde $I_D$ é a corrente no dreno, $V_{GS}$ é a tensão entre a porta e a fonte ($V_G - V_S$), $V_{DS}$ é a tensão entre o dreno e a fonte ($V_D - V_S$), $V_t$ é conhecida como tensão de limiar e $\beta_n$ é um parâmetro intrínseco a cada transistor.

\quad Nota-se que para $V_{DS} = (V_{GS} - V_t)$ o transistor entra em Modo de saturação. Fazendo $$2V \leq V_G \leq 8V$$ $$V_t = 2V$$ $$V_D = 6V$$ $$V_S = 0V$$ podemos ver na Figura \ref{fig:Bn1} o gráfico de $I_D$ em função de $V_{DS}$ e na Figura \ref{fig:Bn1_VGS} o gráfico para $I_D$ em função de $V_{GS}$ para $\frac{\beta_n}{2} = 0,28 \times 10^{-3}$.

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{Bn1.png}
    \caption{Gráfico de ($I_D$ $X$ $V_{DS}$) para $\frac{\beta_n}{2} = ,28 \times 10^{-3}$.}
    \label{fig:Bn1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{Bn1_VGS.png}
    \caption{Gráfico de ($I_D$ $X$ $V_{GS}$) para $\frac{\beta_n}{2} = ,28 \times 10^{-3}$.}
    \label{fig:Bn1_VGS}
\end{figure} 

\section{\textbf{Experiência}}

\quad Iniciamos o experimento montando o circuito da Figura \ref{fig:Circuito_exp} com a tensão no Gate ($V_G$) sendo uma onda senoidal de $0V$ a $5V$, uma tensão de $5V$ no dreno e o resistor $R1 = 100\Omega$. 

\quad A Figura \ref{fig:1b_sinal} mostra a leitura do osciloscópio da tensão $V_G$ de entrada (em amarelo) e da tensão de saída (em verde) que corresponde a tensão na carga R1. Ajustamos o osciloscópio para o modo XY para obter a Figura \ref{fig:1b_XY} que corresponde à curva característica da corrente $I_D$ em função da tensão entre o gate e a fonte $V_{GS}$.

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{Circuito_exp.png}
    \caption{Circuito para experiências 1 e 2.}
    \label{fig:Circuito_exp}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{1b_sinal.jpeg}
    \caption{Sinal de entrada e saída do circuito da figura \ref{fig:Circuito_exp} com a tensão do gate $V_{G}$ variando.}
    \label{fig:1b_sinal}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{1b_XY.jpeg}
    \caption{Curva da corrente $I_D$ em função de $V_{GS}$ do circuito da figura \ref{fig:Circuito_exp}.}
    \label{fig:1b_XY}
\end{figure}

\quad Seguindo o experimento utilizamos o mesmo circuito da Figura \ref{fig:Circuito_exp}, agora teremos a tensão do dreno ($V_D$) fixa em $3V$ e o gate ($V_G$) terá uma senoide de $0V$ à $5V$.

\quad A Figura \ref{fig:2b_sinal} mostra a leitura no osciloscópio da tensão $V_D$ de entrada (em amarelo) e da tensão de saída (em verde) que corresponde a tensão na carga R1. Ajustamos o osciloscópio para o modo XY para obter a Figura \ref{fig:2b_XY} que corresponde curva característica da corrente $I_D$ em função da tensão do dreno $V_{DS}$. 

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{2b_sinal.jpeg}
    \caption{Sinal de entrada e saída do circuito da figura \ref{fig:Circuito_exp} com a tensão do dreno $V_D$ variando.}
    \label{fig:2b_sinal}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{2b_XY.jpeg}
    \caption{Curva da corrente $I_D$ em função de $V_{DS}$ do circuito da figura \ref{fig:Circuito_exp}.}
    \label{fig:2b_XY}
\end{figure}

\section{\textbf{Questões experimentais e discussão}}

\quad O valor de $V_{tn}$ é de $2.4V$, obtivemos esse valor observando os gráficos da Figura \ref{fig:2b_XY}. Pegamos o valor da tensão onde o transistor começa a conduzir, ou seja, entra na região de triodo.

\quad Para encontrar o $\beta_n$ usaremos a equação:
\begin{equation} \label{E1}
I_D = \frac{\beta_n}{2}(V_{GS} - V_{tn})^2
\end{equation}

\quad Para obter o gráfico da Figura \ref{fig:2b_XY} a tensão do gate foi mantida fixa em $V_G = 3V$, como a resistência usada entre a fonte e o dreno foi de $R_{DS} = 100 \Omega$ e sabendo que a tensão de saturação entre o dreno e a fonte é dada por $V_{DSs} = V_{GS} - V_{tn}$ , temos:
$$I_D = \frac{V_{DS}}{R_{DS}}$$
$$I_D = \frac{3 - 2.4}{100}$$
$$I_D = 6 mA$$

\quad Assim, substituindo $I_D$ na equação \ref{E1}, temos que $\beta_n$ é:
$$\beta_n = 0.033A/V^2$$

\quad Para encontrar o $\lambda_n$ usaremos a equação:
\begin{equation} \label{E2}
I_D = \frac{\beta_n}{2}(V_{GS} - V_{tn})^2(1 + \lambda_n V_{DS})
\end{equation}

\quad Novamente, sabendo que no ponto de saturação $V_{DSs} = V_{GS} - V_{tn}$, temos:
$$I_D = \frac{\beta_n}{2}(V_{GS} - V_{tn})^2(1 + \lambda_n (V_{GS} - V_{tn}))$$

\quad Usando o valor de $\beta_n$ encontrado anteriormente e sabendo que para o ponto de saturação $I_D = 6 mA$, vemos que:
$$\lambda_n = -0.825$$

\section{\textbf{Referências}}
[1] A.S. Sedra e K. Smith, Microeletronica, 5a ed. Pearson, Brasil, 2007.

[2] $http://hardplus.com.br/blog/$

[3] $http://lilith.fisica.ufmg.br/~labexp$

[4] $http://www.newtoncbraga.com.br/index.php$

\bibliographystyle{abbrv}
\end{document}

%http://fhollweg.xpg.uol.com.br/eletronica/eletronica_cap2.pdf
%http://lilith.fisica.ufmg.br/~labexp/roteirosPDF/Diodo_semicondutor.pdf

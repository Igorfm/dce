\documentclass[conference, 12pt]{IEEEtran}
\usepackage{graphicx}			% Inclusão de gráficos
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amssymb}
\bibliographystyle{abnt-num}

\title{Amplificadores operacionais:\\Circuitos de detecção de imperfeições, integrador e seguidor}
\author{\IEEEauthorblockN {Andrei Alexei Levy Buslik} \IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB} \and \IEEEauthorblockN {Igor Fernandes Miranda}
\IEEEauthorblockA{Engenharia de Computação \\ Universidade de Brasília - UnB}}


\begin{document}
\maketitle
\abstractname{--}\textbf{Este relatório visa comparar, através de cálculos, simulações e experimentos práticos, circuitos que utilizam amplificadores operacionais. Inicialmente é descrito o seu comportamento tendo em vista a frequência e a largura de banda do sinal de entrada e a resposta em frequência na saída do amplificador, em seguida é mostrada uma simulação de um circuito, cálculos para resultados esperados em e, por fim, comparam-se os resultados teóricos e simulados com os resultados dos experimentos em laboratório.}

\section{\textbf{Introdução}}

\quad O comportamento em altas frequências de um amplificador operacional é importante em muitas aplicações. Este comportamento é caracterizado pela resposta em frequência do amplificador operacional onde a largura de banda a pequenos sinais é definida como sendo a frequência de ganho unitário que excede 1GHz em processos CMOS atuais. A frequência de corte pode também ser especificada para predição da resposta em frequência em malha fechada.

\quad A resposta em frequência de um sistema é definida como a resposta em regime estacionário, quando aquele é sujeito a uma entrada senoidal. As principais vantagens da abordagem de resposta em frequência são:

\begin{itemize}
\item Facilidade com que a resposta em frequência de um sistema pode ser obtida; 
\item Possibilita  determinar a função de transferência de determinados sistemas; 
\item Possibilita analisar a estabilidade absoluta e relativa de um sistema, mesmo quando se desconhece a sua função de transferência em cadeia fechada; 
\item Possibilita projetar um sistema de controle, ainda que se desconheça a função de transferência;
\item O projeto de sistemas de controle no domínio da frequência permite ao projetista controlar a largura de banda e minimizar os efeitos do ruído a que o sistema está sujeito;
\item Existência de uma relação, no mínimo indireta, entre a resposta em frequência e a resposta transitória.  
\end{itemize}

\quad O gráfico onde é analisada a resposta em frequência de uma rede é o Diagrama de Bode. Ele possui uma escala linear de ganho na ordenada em decibéis (dB) ou em Volt por Volt (V/V). 

\quad O Diagrama de Bode é composto por dois gráficos. O primeiro representa a curva do módulo de $G(j\omega)$ em decibéis e o segundo representa a curva da fase, sendo ambos função da frequência em escala logarítmica. 

\section{\textbf{Questões teóricas e simulação}}

\quad Nessa sessão serão respondidas duas questões sobre resposta em frequência do amp-op. A primeira questão pede para determinar a funcão de transferência $V_o(s)/V_1(s)$ do circuito ilustrado na Figura \ref{fig:Circuito_teoria}, considerando $\frac{1}{A_0} \approx 0$. Também foi pedida a frequência de corte ($f_c$) do circuito.

\begin{figure}[htpb]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{Circuito_teoria.png}
    \caption{Circuito amplifcador não-inversor com compensação para efeito da corrente de polarização.}
    \label{fig:Circuito_teoria}
\end{figure}

\quad Considerando que $A(S) = \frac{A_0}{1 + \frac{S}{\omega_b}}$, foram obtidos os seguintes valores:

\begin{equation}
\frac{V_o}{V_1} = \frac{1+\frac{R_2}{R_1}}{1+\frac{1+\frac{R_2}{R_1}}{A}} = \frac{A_0 \omega_b}{S + \omega_b(1 + A_0 \frac{R_1}{R_1 + R_2})} \label{E1}
\end{equation}
\begin{equation}
\omega_c = \omega_b(1 + A_0 \frac{R_1}{R_1 + R_2}) \label{E2}
\end{equation}

O diagrama de bode do circuito da \ref{fig:Circuito_teoria} com ganho 10 é apresentado na \ref{fig:Bode}

\begin{figure}[htpb]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.5]{bode.png}
    \caption{Diagrama de Bode Circuito amplifcador não-inversor com compensação para efeito da corrente de polarização.}
    \label{fig:Bode}
\end{figure}

O ganho do circuito não frequência de corte é de 0.707, ou seja, temos uma atenuação de 29,93\%.

\section{\textbf{Experiência}}

\quad Na primeira parte do experimento foi montado o circuito da Figura \ref{fig:Circuito_teoria} utilizando resistores $R_1 = 1 k\Omega$ e $R_2 = 10 k\Omega$, uma onda senoidal de $500 mV$ de amplitude na entrada $V_1$ e frequência de $1 kHz$, e a CI TL074 (esse circuito será denominado como \textit{Cicuito 1}). As Figuras \ref{fig:E1a_Amplitude} e \ref{fig:E1a_Fase} mostram as tensões de entrada (em amarelo) e de saída (em verde). Após as tensões de entrada e saída terem sido capturadas no osciloscópio, a frequência do sinal de entrada foi aumentada até se perceber que a tensão de pico-a-pico ($V_{pp}$) para a saída ficou em torno de $7,07 V$, o que representa uma queda de $3 dB$ na tensão da saída, a frequência de corte obtida foi de: 

$$f_{3 dB} = 346 kHz$$

\quad Utilizando os cursores do osciloscópio, vemos pelas Figuras \ref{fig:E1a_Amplitude} e \ref{fig:E1a_Fase} que as tensões máximas para a entrada e saída e a diferença de fase são:

$$V_1 = 480 mV$$
$$V_o = 940 mV$$
$$\frac{1}{|\Delta t|} = 1,786 Mhz$$

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{E1a_Amplitude.pdf}
    \caption{Formas de onda das tensões de entrada e saída e suas tensões máximas para sinal com frequêcia de $1 kHz$.}
    \label{fig:E1a_Amplitude}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{E1a_Fase.pdf}
    \caption{Formas de onda das tensões de entrada e saída e a diferença de fase entre elas.}
    \label{fig:E1a_Fase}
\end{figure}

\quad Seguindo o experimento, a frequência do \textit{Circuito 1} foi mudada para as frequências de $f = 0,1 f_{3dB}$ e $f = 10 f_{3dB}$ em ordem. Pelas Figuras \ref{fig:E1b_01f_Amplitude} e \ref{fig:E1b_01f_Fase} podemos ver que as tensões máximas para a entrada e saída e a diferença de fase para a frequência de $f = 0,1 f_{3dB}$ são:

$$V_1 = 496 mV$$
$$V_o = 552 mV$$
$$\frac{1}{|\Delta t|} = 625 kHz$$

\quad As Figuras \ref{fig:E1b_10f_Amplitude} e \ref{fig:E1b_10f_Fase} mostram que as tensões máximas para a entrada e saída e a diferença de fase para a frequência de $f = 10 f_{3dB}$ são:

$$V_1 = 480 mV$$
$$V_o = 184 mV$$
$$\frac{1}{|\Delta t|} = 7,813 MHz$$

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{E1b_01f_Amplitude.pdf}
    \caption{Formas de onda das tensões de entrada e saída e suas tensões máximas para sinal com frequência de $0,1 f_{3 dB}$.}
    \label{fig:E1b_01f_Amplitude}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{E1b_01f_Fase.pdf}
    \caption{Formas de onda das tensões de entrada e saída e a fase entre elas para sinal com frequência de $0,1 f_{3 dB}$.}
    \label{fig:E1b_01f_Fase}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{E1b_10f_Amplitude.pdf}
    \caption{Formas de onda das tensões de entrada e saída e suas tensões máximas para sinal com frequência de $10 f_{3 dB}$}
    \label{fig:E1b_10f_Amplitude}
\end{figure}
    
\begin{figure}[h]
    \centering
    \includegraphics[keepaspectratio=true, scale=0.7]{E1b_10f_Fase.pdf}
    \caption{Formas de onda das tensões de entrada e saída e a fase entre elas para sinal com frequência de $10 f_{3 dB}$.}
    \label{fig:E1b_10f_Fase}
\end{figure}

\quad Na última parte do experimento foi trocado no \textit{Circuito 1} o resistor $R_2 = 10 k\Omega$ por um de $100 k\Omega$ e a tensão $V_{pp}$ da entrada senoidal foi mudada para $50 mV$ afim de se evitar saturação na saída (esse circuito é denominado \textit{Circuito 2}). Seguindo os mesmos procedimentos do \textit{Circuito 1} foi encontrada uma frequência de corte de:

$$f_c = 30,1 kHz$$

\section{\textbf{Questões experimentais e discussão}}

\quad A relação entre a frequência de transição e a frequência de corte é dada pela equação \ref{E3}.  

\begin{equation}
f_t = f_c \times \frac{R_1 + R_2}{R_1} \label{E3}
\end{equation}

\quad Usando como referência o \textit{datasheet} do fabricante, a CI TL074 possui frequência de transição de $3 MHz$. Dessa forma, a frequência de corte esperada para o \textit{Circuito 1} seria:

$$f_{teorica1} = 272,72 kHz$$

\quad Uma vez que a frequência de corte encontrada no experimento foi de $346 kHz$ temos uma discrepância de 26\% entre a frequência experimental e a teórica.

\quad O experimento mostrou que a frequência de corte do \textit{Circuito 1} foi mais de 10 vezes a do \textit{Circuito 2}. Isso ocorre porque, como podemos ver pela equação \ref{E2}, a frequência de corte depende do valor dos resitores, como trocamos o resistor $R_2$ do \textit{Circuito 1} por um com valor 10 vezes maior no \textit{Circuito 2} era esperado que a frequência de corte ficasse 10 vezes menor.

\quad Assim como foi feito para o \textit{Circuito 1}, o valor esperado da frequência de corte para o \textit{Circuito 2} é:

$$f_{teorica2} = 29,7 kHz$$

\quad A frequência de corte experimental para esse circuito foi de $30,1 kHz$, nota-se uma discrepância de apenas 1\%.

\section{\textbf{Referências}}
[1] A.S. Sedra e K. Smith, Microeletronica, 5a ed. Pearson, Brasil, 2007.


\bibliographystyle{abbrv}
\end{document}
